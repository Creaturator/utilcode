﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;

namespace CSharp {

    internal class UpdateChecker {

        [NonSerialized]
        public Uri? url;
        [NonSerialized]
        public const string localVersion = "Prototype 1.0";
        public static string updateURL =
        "https://creaturatorappversions.azurewebsites.net/api/creatures-in-a-box?code=bKOpVPxl7zYF3UhzGFOM_DI44TsIMlNwwnP-Zfr0wCwqAzFuy4kpvQ==";

        private static UpdateChecker? instanceVariable;

        private UpdateChecker() {}

        public async Task<bool> checkForUpdate() {
            url = new Uri(updateURL);
            string serverVersion = "";
            using (HttpClient downloader = new HttpClient()) {
                try {
                    serverVersion = await downloader.GetStringAsync(url);
                } catch (Exception e) {
                    throw new HttpRequestException("Download failed.", e);
                }
            }
            return serverVersion == localVersion;
        }

		/**
		*	Getter to access this Class by Singleton Pattern.
		*/
        public static UpdateChecker instance() => instanceVariable ??= new UpdateChecker();
    }
}