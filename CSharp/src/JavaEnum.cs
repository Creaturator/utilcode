using System;
using System.Linq;
using System.Reflection;

namespace StoreScout.Api.Extensions
{
    public abstract class JavaEnum
    {
        #region ID

        private readonly int id;

        private static int runningNumber = 0;

        protected JavaEnum()
        {
            id = runningNumber++;
        }

        public int getID() => id;

        #endregion

        #region Methods

        public static Tuple<string, H?>[] values<H>() where H : JavaEnum
        {
            FieldInfo[] temp = typeof(H).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.GetField)
                .Where(f => f.GetValue(null) != null).ToArray();
            Tuple<string, H?>[] array = new Tuple<string, H?>[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                array[i] = new Tuple<string, H?>(temp[i].Name, temp[i].GetValue(null) as H);
            }

            return array;
        }

        public static bool tryParse<H>(string nameOfField, out H? parsedObj) where H : JavaEnum
        {
            try
            {
                Tuple<string, H?>[] valueTuples = values<H>();
                if (valueTuples.All(h => !h.Item1.Equals(nameOfField)))
                {
                    parsedObj = null;
                    return false;
                }

                parsedObj = valueTuples.First(t => t.Item1.Equals(nameOfField)).Item2;
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                parsedObj = null;
                return false;
            }
        }

        #endregion
    }
}