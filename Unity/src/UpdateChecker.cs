﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Unity {

    [SuppressMessage("ReSharper", "ConvertToUsingDeclaration")]
    internal class UpdateChecker<C> where C : MonoBehaviour {

        [NonSerialized]
        public Uri url;
        [NonSerialized]
        public const string currVersion = "Prototype 1.0";
        public static string updateURL = "https://www.dropbox.com/s/vpcgozhj76eib8o/GameUpdateCheck.txt?dl=1";

        private const string spielName = "Creatures in a box";
        private UnityWebRequest.Result returnMessage;
        private bool updateAvailable;
        private string newestVersion;
        private static UpdateChecker<C> instanceVariable;

        private UpdateChecker() {
            returnMessage = UnityWebRequest.Result.DataProcessingError;
            newestVersion = "";
            updateAvailable = false;
        }

		/**
		* Initializes the async check for updates
		**/
        // ReSharper disable once ParameterHidesMember
        public void CheckForUpdates(C caller) {
            caller.StartCoroutine(actualCheck());
        }


        /**
		*	Makes the updatecheck by sending a Get Request using UnityWebRequest.Get. 
		* 	Then checks if everything was processed right and if so checks the downloaded file, looks for the game version and checks it.
		**/
        private IEnumerator actualCheck() {
            if (!Application.isPlaying) { }
            else {
	            url = new Uri(updateURL);
                using (UnityWebRequest request = UnityWebRequest.Get(url)) {
                    yield return request.SendWebRequest();

                    setResult(request.result);
                    if (request.result != UnityWebRequest.Result.Success)
                    {
                        Debug.LogError(request.error);
                    }
                    else
                    {
                        string datei = request.downloadHandler.text;
                        int beginSubstring = datei.IndexOf(spielName, StringComparison.CurrentCulture) +
                                             spielName.Length + 2;
                        int indexEnde = datei.IndexOf("\r\n", beginSubstring, StringComparison.CurrentCulture);
                        string newVersion = datei.Substring(beginSubstring, indexEnde - beginSubstring);
                        if (!newVersion.Equals(currVersion))
                            setNewVersionAvailable(newVersion);
                    }
                }
            }
        }

        /**
		*	Sets sync the result of the WebRequest.
		**/
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void setResult(UnityWebRequest.Result result) => returnMessage = result;

		/**
		*	Sets sync if a new game version is available.
		**/
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void setNewVersionAvailable(string newVersion) {
            updateAvailable = true;
            newestVersion = newVersion;
            setResult(UnityWebRequest.Result.Success);
        }

		/**
		*	Composes the result, the newest version and if there is a newer version available.
		**/
        [MethodImpl(MethodImplOptions.Synchronized)]
        public Tuple<bool, Tuple<string, UnityWebRequest.Result>> isNewVersionAvailable() =>
            new Tuple<bool, Tuple<string, UnityWebRequest.Result>>(updateAvailable, new Tuple<string, UnityWebRequest.Result>
                (newestVersion, returnMessage));

		/**
		*	Getter to access this Class by Singleton Pattern.
		*/
        public static UpdateChecker<C> instance() => instanceVariable = instanceVariable ?? new UpdateChecker<C>();
    }
}