﻿using UnityEngine;

namespace Unity {
    internal class Timer {
        private float timer = -0.1f;

        public bool isRunning() {
            if (timer > 0.03f) {
                timer -= Time.deltaTime;
                return true;
            }
            if (timer <= 0.03f && timer > -0.1f)
                timer = -0.1f;
            return false;
        }

        public void startTimer(float time) => timer = time;

        public void update(string defaultText, TextMesh toCheckAgainst) {
            if(!isRunning() && !toCheckAgainst.text.Equals(defaultText))
               toCheckAgainst.text = defaultText;
        }
    }
}